<?php 
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['newPetsUid'];
$defaultImage = $_POST['default_image'];
$puppyUpdated = updateDynamicData($conn,"puppy","WHERE uid =?",array("default_image"),array($defaultImage,$uid), "ss");
$petsUpdated = updateDynamicData($conn,"pet_details","WHERE uid =?",array("default_image"),array($defaultImage,$uid), "ss");

if ($puppyUpdated) {
	$_SESSION['messageType'] = 1;
	header('location: ../addPuppy.php');
}else{
	echo "error";
}
unset($_SESSION['puppy_uid']);unset($_SESSION['newPetsUid']);unset($_SESSION['image']);
 ?>