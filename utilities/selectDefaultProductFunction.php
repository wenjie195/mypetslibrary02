<?php 
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['newProduct_uid'];
$defaultImage = $_POST['default_image'];
$productUpdated = updateDynamicData($conn,"product","WHERE uid =?",array("default_image"),array($defaultImage,$uid), "ss");

if ($productUpdated) {
	$_SESSION['messageType'] = 1;
    header('location: ../addVariations.php');
}else{
	echo "error";
}

unset($_SESSION['product_uid']);
//unset($_SESSION['newProduct_uid']);
unset($_SESSION['image']);
?>