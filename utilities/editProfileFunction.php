<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $name = rewrite($_POST["update_name"]);
    // $gender = rewrite($_POST["update_gender"]);
    $dob = rewrite($_POST["update_dob"]);
    $phone = rewrite($_POST["update_phone"]);
    $email = rewrite($_POST["update_email"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";
    // echo $gender."<br>";
    // echo $dob."<br>";
    // echo $phone."<br>";
    // echo $email."<br>";

    $user = getUser($conn," uid = ?   ",array("uid"),array($uid),"s");    

    if(!$user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        // if($gender)
        // {
        //     array_push($tableName,"gender");
        //     array_push($tableValue,$gender);
        //     $stringType .=  "s";
        // }
        if($dob)
        {
            array_push($tableName,"birthday");
            array_push($tableValue,$dob);
            $stringType .=  "s";
        }
        if($phone)
        {
            array_push($tableName,"phone_no");
            array_push($tableValue,$phone);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            // header('Location: ../editProfile.php?type=1');
            header('Location: ../profile.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editProfile.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editProfile.php?type=3');
    }

}
else 
{
    header('Location: ../index.php');
}
?>
