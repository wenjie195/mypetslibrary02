<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CreditCard.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $cardNumber = rewrite($_POST["card_number"]);
    $cardUser = rewrite($_POST["card_user"]);
    $status = "Delete";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $cardNumber."<br>";
    // echo $cardUser."<br>";

    if(isset($_POST['card_number']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }    

        // array_push($tableValue,$cardNumber);
        // $stringType .=  "s";
        // $deleteCreditCard = updateDynamicData($conn,"credit_card"," WHERE card_no = ? ",$tableName,$tableValue,$stringType);

        array_push($tableValue,$cardNumber,$cardUser);
        $stringType .=  "ss";
        $deleteCreditCard = updateDynamicData($conn,"credit_card"," WHERE card_no = ? AND username = ? ",$tableName,$tableValue,$stringType);
        if($deleteCreditCard)
        {
            // echo "credit card deleted";
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=8');
        }
        else
        {
            // echo "fail";
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=10');
        }
    }
    else
    {
        // echo "error";
        $_SESSION['messageType'] = 1;
        header('Location: ../editBankDetails.php?type=6');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>
