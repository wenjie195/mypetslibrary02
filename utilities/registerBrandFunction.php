<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Brand.php';
require_once dirname(__FILE__) . '/../classes/User.php';


require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewBrand($conn,$name,$status)
{
     if(insertDynamicData($conn,"brand",array("name","status"),
          array($name,$status),"ss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $name = rewrite($_POST['register_name']);
    $status = rewrite($_POST['register_status']);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $name."<br>";
    // echo $status."<br>";

    $brandRows = getBrand($conn," WHERE name = ? AND status= 'Available' ",array("name"),array($name),"s");
    $existingBrand = $brandRows[0];

    if(!$existingBrand)
    {
        if(registerNewBrand($conn,$name,$status))
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../brand.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addBrand.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../addBrand.php?type=3');
    }
}
else 
{
     header('Location: ../index.php');
}
?>