<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Breed.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $breedName = rewrite($_POST["update_breed_name"]);
    $id = rewrite($_POST["breed_id"]);

    $previousType = rewrite($_POST["breed_type"]);

    $allBreed = getBreed($conn," WHERE name = ? AND type = ? ",array("name","type"),array($breedName,$previousType),"si");
    $existingBreed = $allBreed[0];

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $breed = getBreed($conn," id = ?   ",array("id"),array($id),"s");    

    if($previousType == '1')
    {
        if(!$existingBreed)
        {
            if(!$breed)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($breedName)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$breedName);
                    $stringType .=  "s";
                }
                array_push($tableValue,$id);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"breed"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../puppyBreed.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../puppyBreed.php?type=2');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../puppyBreed.php?type=3');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../puppyBreed.php?type=4');
        }
    }
    elseif($previousType == '2')
    {
        if(!$existingBreed)
        {
            if(!$breed)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($breedName)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$breedName);
                    $stringType .=  "s";
                }
                array_push($tableValue,$id);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"breed"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kittenBreed.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../kittenBreed.php?type=2');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../kittenBreed.php?type=3');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../kittenBreed.php?type=4');
        }
    }
    elseif($previousType == '3')
    {
        if(!$existingBreed)
        {
            if(!$breed)
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($breedName)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$breedName);
                    $stringType .=  "s";
                }
                array_push($tableValue,$id);
                $stringType .=  "s";
                $passwordUpdated = updateDynamicData($conn,"breed"," WHERE id = ? ",$tableName,$tableValue,$stringType);
                if($passwordUpdated)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reptileBreed.php?type=1');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reptileBreed.php?type=2');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../reptileBreed.php?type=3');
            }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../reptileBreed.php?type=4');
        }
    }
}
else 
{
    header('Location: ../index.php');
}
?>
