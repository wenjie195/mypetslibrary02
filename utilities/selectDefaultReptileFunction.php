<?php 
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$uid = $_SESSION['newPetsUid'];
$defaultImage = $_POST['default_image'];
$reptileUpdated = updateDynamicData($conn,"reptile","WHERE uid =?",array("default_image"),array($defaultImage,$uid), "ss");
$petsUpdated = updateDynamicData($conn,"pet_details","WHERE uid =?",array("default_image"),array($defaultImage,$uid), "ss");

if ($reptileUpdated) {
	$_SESSION['messageType'] = 1;
	header('location: ../addReptile.php');
}else{
	echo "error";
}
unset($_SESSION['reptile_uid']);unset($_SESSION['newPetsUid']);unset($_SESSION['image']);
 ?>