<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Brand.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$newProductUid = $_SESSION['newProduct_uid'];
//$newProductUid = $_GET['newProduct_uid'];

$conn = connDB();

$categoryDetails = getCategory($conn);
$brandDetails = getBrand($conn);

$productDetails = getProduct($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Product | Mypetslibrary" />
<title>Add New Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<!-- <//?php include 'userHeaderAfterLogin.php'; ?>
<//?php include 'header.php'; ?> -->

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add Product Free Gift</h1>
            <div class="green-border"></div>
    </div>
    <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/ProductGiftFunction.php" method="POST" enctype="multipart/form-data">
         <?php 
            if($newProductUid){
                $conn = connDB();
                $product = getProduct($conn,"WHERE uid = ? ", array("uid") ,array($newProductUid),"s");
                ?>
                <div>
                    <input type="hidden" value="<?php echo $newProductUid ?>" name="register_product_uid" id="register_product_uid">
                </div>
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p slug-p">Free Gift</p>
                    <select class="input-name clean admin-input" name="register_free_gift" id="register_free_gift" onchange='FreeGift(this.value);' required> 
                        <option>Free Gift Availability</option>  
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <div class="dual-input second-dual-input" name="fg_desc" id="fg_desc" style='display:none;'>
                    <p class="input-top-p admin-top-p">Free Gift Description</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Free Gift Description" name="register_freeGift_desc" id="register_freeGift_desc">
                </div>
                <div class="clear"></div>

                <div class="width100 overflow text-center">
                    <button class="green-button white-text clean2 edit-1-btn margin-auto">Add Image</button>
                </div>
                <?php
            }
            ?>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new product!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Registration of new seller failed!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

<script type="text/javascript">
function FreeGift(val){
 //var element=document.getElementById('fg_img');
 var elementA=document.getElementById('fg_desc');
 if(val=='Free Gift Availability'||val=='yes'){
   //element.style.display='block';
   elementA.style.display='block';
 }
 else {
   element.style.display='none';
 }
}

</script> 

</body>
</html>
