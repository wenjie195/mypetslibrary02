<?php

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

//require_once dirname(__FILE__) . '/classes/Product1.php';
require_once dirname(__FILE__) . '/classes/Variation2.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    header('Location: ./cart.php');
}

if(isset($_GET['id'])){
    $referrerUidLink = $_GET['id'];
}
else{
    echo "error";
}

//$products = getProduct($conn, "WHERE status = 'Available' ORDER BY date_created DESC ");
//$products = getProduct($conn);
$variation = getVariation($conn);

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($variation,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($variation);
    }
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pedigree Dentastix Dog Treats | Mypetslibrary" />
<title>Pedigree Dentastix Dog Treats | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet.Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet. Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 menu-distance3 same-padding min-height2">
    <?php
        $conn = connDB();
            if($variation)
            {
                for($cnt = 0;$cnt < count($variation) ;$cnt++)
                {
                    if($variation[$cnt]->getUid()==$referrerUidLink)
                    {
                    ?>
                        <div class="left-image-div">
                            <div class="item">
                                <div class="clearfix">
                                    <img src="<?php echo "uploads/".$variation[$cnt]->getVariationImage() ?>" class="pet-slider-img" alt="Product Name" title="Product Name" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="right-content-div2">
                            <p class="green-text breed-p"><?php echo $variation[$cnt]->getVariation();?></p>
                            <h1 class="green-text pet-name"><?php echo $variation[$cnt]->getBrand()." ".$variation[$cnt]->getVariation()." (".$variation[$cnt]->getAnimalType().") ";?>Product For Sale</h1>
                            <p class="price-p2">
                                RM<?php echo $variation[$cnt]->getVariationPrice();?>.00
                            </p>
                            <div class="right-info-div">
                                <a href="tel:+60383190000" class="contact-icon hover1">
                                    <img src="img/call.png" class="hover1a" alt="Call" title="Call">
                                    <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
                                </a>
                                <!-- <a onclick="window.open(this.href); return false;" href="https://api.whatsapp.com/send?phone=6<//?php echo $adminData->getPhoneNo();?>" class="contact-icon hover1">
                                    <img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                                    <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
                                </a>   -->
                                <a class="contact-icon hover1">
                                    <img src="img/favourite-1.png" class="hover1a" alt="Favourite" title="Favourite">
                                    <img src="img/favourite-2.png" class="hover1b" alt="Favourite" title="Favourite">
                                </a>  

                                <a class="contact-icon hover1 last-contact-icon open-social">
                                    <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                                    <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
                                </a>
                            </div>
                            <div class="clear"></div>

                            <div class="pet-details-div">
                                <div class="tab">
                                    <button class="tablinks active" onclick="openTab(event, 'Details')">Details</button>
                                    <button class="tablinks" onclick="openTab(event, 'Terms')">Terms</button>
                                </div>
                                <div  id="Details" class="tabcontent block">
                                    <table class="pet-table">
                                        <tr>
                                            <td class="grey-p">SKU</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $variation[$cnt]->getSku();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">Stock</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $variation[$cnt]->getVariationStock();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">Expiry Date</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $variation[$cnt]->getExpiryDate();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">For Animal Type</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $variation[$cnt]->getAnimalType();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">Category</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $variation[$cnt]->getCategory();?></td>
                                        </tr>
                                        <tr>
                                            <td class="grey-p">Description</td>
                                            <td class="grey-p">:</td>
                                            <td><?php echo $variation[$cnt]->getDescription();?></td>
                                        </tr>
                                    </table>
                                    
                                </div>

                                <div id="Terms" class="tabcontent">
                                    <p class="pet-table-p">Terms content</p>
                                </div>
                                <a href="malaysia-pet-food-toy-product.php">
                                    <div class="review-div hover1 upper-review-div">
                                        <p class="left-review-p grey-p">Brand</p>
                                        <p class="left-review-mark brand-p text-overflow"><?php echo $variation[$cnt]->getBrand();?></p>

                                        <p class="right-arrow">
                                            <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                                            <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                                        </p>
                                        <p class="beside-right-arrow grey-to-lightgreen">
                                            View All Product
                                        </p>
                                    </div>
                                </a>
                                <a href="productReview.php">
                                    <div class="review-div hover1 lower-review-div">
                                        <p class="left-review-p grey-p">Reviews</p>
                                        <p class="left-review-mark">4/5</p>
                                        <p class="right-review-star">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                                            <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                                        </p>
                                        <p class="right-arrow">
                                            <img src="img/arrow.png" alt="Review" title="Review" class="arrow-img hover1a">
                                            <img src="img/arrow2.png" alt="Review" title="Review" class="arrow-img hover1b">
                                        </p>
                                    </div>
                                </a>
                                <div class="width100 small-padding overflow min-height-with-filter filter-distance product-filter-distance">
                                    <form method="POST">
                                        <div >
                                            <?php echo $productListHtml; ?>
                                        </div>
                                        <div class="clear"></div>  
                                        <div class="width100 text-center">                                                                         
                                            <button class="green-button checkout-btn clean">Add to Cart</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> 
                    <?php
                    }
                }
            }
    ?>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<div class="sticky-distance2 width100"></div>

<div class="sticky-call-div">
<table class="width100 sticky-table">
	<tbody>
    	<tr>
        	<td>
                <a  href="tel:+60383190000" class="text-center clean transparent-button same-text" >
                    Call
                </a>
        	</td>
            <td>
                <a onclick="window.open(this.href); return false;" href="https://api.whatsapp.com/send?phone=601159118132"  class="text-center clean transparent-button same-text">
                    Whatsapp
                </a>
        	</td>
    	</tr>
    </tbody>    
</table>   
</div>

<?php include 'stickyFooter.php'; ?>

<style>
.social-dropdown {
    width: 360px;
}
</style>

<script>
    $(".button-minus").on("click", function(e)
    {
        e.preventDefault();
        var $this = $(this);
        var $input = $this.closest("div").find("input");
        var value = parseInt($input.val());
        if (value > 1)
        {
            value = value - 1;
        } 
        else 
        {
            value = 0;
        }
        $input.val(value);
    });

    $(".button-plus").on("click", function(e)
    {
    e.preventDefault();
    var $this = $(this);
    var $input = $this.closest("div").find("input");
    var value = parseInt($input.val());
    if (value < 100)
    {
        value = value + 1;
    }
    else
    {
        value = 100;
    }
    $input.val(value);
    });
</script>

</body>
</html>
