<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Product | Mypetslibrary" />
<title>Edit Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <//?php include 'userHeaderAfterLogin.php'; ?> -->
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit Variation</h1>
            <div class="green-border"></div>
    </div>
    <div class="border-separation">
        <div class="clear"></div>
        <!-- <//?php echo $_POST['variation_id']; ?> -->
        <form method="POST" action="utilities/editVariationFunction.php" enctype="multipart/form-data">
        <?php
            if(isset($_POST['variation_id']))
            {
                $conn = connDB();
                $variationDetails = getVariation($conn,"WHERE id = ? ", array("id") ,array($_POST['variation_id']),"i");
            ?>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Variation Name(RM)*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $variationDetails[0]->getVariation();?>" required name="update_variation" id="update_variation">      
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Variation Price*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $variationDetails[0]->getVariationPrice();?>" required name="update_variation_price" id="update_variation_price">      
                </div>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Variation Stock*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $variationDetails[0]->getVariationStock();?>" required name="update_variation_stock" id="update_variation_stock">      
                </div>

                <input type="hidden" id="uid" name="uid" value="<?php echo $variationDetails[0]->getUid() ?>">

                <div class="clear"></div>  
                
                <div class="width100 overflow text-center">     
                    <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="editSubmit" name ="editSubmit">Submit</button>
                </div>
                <div class="clear"></div>
        </form>

        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Variation Photo</p>
            <div class="four-div-box1">
                <img src="uploads/<?php echo $variationDetails[0]->getVariationImage();?>" class="pet-photo-preview">
                <form method="POST" action="updateVariationImageOne.php" class="hover1" target="_blank">
                    <button class="clean green-button pointer width100" type="submit" name="product_uid" value="<?php echo $variationDetails[0]->getUid();?>">
                    Update Image 1
                    </button>
                </form>
            </div>                    
        </div>
        
        <?php
        }
        ?>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>