<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Reviews.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller Review | Mypetslibrary" />
<title>Pet Seller Review | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

    <div class="width100 same-padding overflow min-height menu-distance2">

        <?php
        if(isset($_GET['id']))
        {
            $conn = connDB();
            // $articlesDetails = getArticles($conn,"WHERE article_link = ? ", array("article_link") ,array($_GET['id']),"s");
            $reviewDetails = getReviews($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
            if($reviewDetails)
            {
                for($cnt = 0;$cnt < count($reviewDetails) ;$cnt++)
                {
                ?>
                	<h1 class="green-text user-title">You're Going to Report the Following Review</h1>
                    <!--<h1 class="green-text user-title ow-margin-bottom-0"><?php echo $reviewDetails[$cnt]->getTitle();?></h1>-->      
                    <p class="author-p ow-margin-bottom-0"><?php echo $reviewDetails[$cnt]->getAuthorName();?> <span class="small-blog-date">(<?php echo $date = date("d-m-Y",strtotime($reviewDetails[$cnt]->getDateCreated()));?>)</span></p>
                    
                    <p class="article-paragraph margin-bottom30 ow-no-margin-top">
                        <?php echo $reviewDetails[$cnt]->getParagraphOne();?>
                    </p>
                                   
                                <form method="POST" action="utilities/reportReviewFunction.php">
                                    
                                    <input class="input-name clean" type="text" placeholder="Reason of Reporting the Review" name="report_reason" id="report_reason" required>   
                                    <input class="input-name clean" type="hidden" value="<?php echo $reviewDetails[$cnt]->getUid();?>" name="review_uid" id="review_uid" required readonly>   
                                    <div class="clear"></div>
                                    <button class="green-button white-text width100 clean2" name="submit">Report</button>
                                </form>  



                    </h1>


                <?php
                }
            }
            ?>
        <?php
        }
        ?>

    </div>


<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>